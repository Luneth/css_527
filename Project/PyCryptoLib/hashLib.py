# Seth Pham
# University of Washington
# CSS 527
# Brent Lagesse
# Project: Cryptography Library Analysis

import hashlib


# -------------------------------------
# Hash Function (MD5, SHA512)
# -------------------------------------

def MD5(text):
    hashText = hashlib.md5(text).hexdigest()
    return hashText


def SHA512(text):
    hashText = hashlib.sha512(text).hexdigest()
    return hashText
