# Seth Pham
# University of Washington
# CSS 527
# Brent Lagesse
# Project: Cryptography Library Analysis

from Cryptodome.Cipher import AES, ARC4, ChaCha20, DES3, PKCS1_OAEP
from Cryptodome.Hash import SHA, SHA256
from Cryptodome.PublicKey import ECC, RSA
from Cryptodome.Random import get_random_bytes
from Cryptodome.Signature import DSS

from struct import pack


# -------------------------------------
# Block Ciphers (AES, 3DES)
# -------------------------------------

def AES_encrypt(text):
    bs = AES.block_size
    key = get_random_bytes(16)
    iv = get_random_bytes(16)
    cipher = AES.new(key, AES.MODE_CBC, iv)
    padLen = bs - len(text) % bs
    padding = [padLen] * padLen
    padding = pack('b' * padLen, *padding)
    cipherText = cipher.encrypt(text + padding)
    return (key, iv, cipherText)


def AES_decrypt(data):
    (key, iv, cipherText) = data
    decodeCipher = AES.new(key, AES.MODE_CBC, iv)
    plainText = decodeCipher.decrypt(cipherText)
    return plainText


# Added spaces due to 3DES messing up the first 8 bytes every time
def DES_encrypt(text):
    # Avoid Option 3
    while True:
        try:
            key = DES3.adjust_key_parity(get_random_bytes(24))
            break
        except ValueError:
            pass

    bs = DES3.block_size
    key = get_random_bytes(16)
    padLen = bs - len(text) % bs
    padding = [padLen] * padLen
    padding = pack('b' * padLen, *padding)
    spaces = b" " * 8
    cipher = DES3.new(key, DES3.MODE_CFB)
    cipherText = cipher.encrypt(spaces + text + padding)
    return (key, cipherText)


def DES_decrypt(data):
    (key, cipherText) = data
    decodeCipher = DES3.new(key, DES3.MODE_CFB)
    plainText = decodeCipher.decrypt(cipherText)
    return plainText


# -------------------------------------
# Stream Cipher (ChaCha 20, RC4)
# -------------------------------------

def ChaCha20_encrypt(text):
    key = get_random_bytes(32)
    encryptor = ChaCha20.new(key=key)
    temp = encryptor.nonce + encryptor.encrypt(text)
    nonce = temp[:8]
    cipherText = temp[8:]
    return (key, nonce, cipherText)


def ChaCha20_decrypt(data):
    (key, nonce, cipherText) = data
    decryptor = ChaCha20.new(key=key, nonce=nonce)
    plainText = decryptor.decrypt(cipherText)
    return plainText


def RC4_encrypt(text):
    key = get_random_bytes(32)
    encryptor = ARC4.new(key)
    cipherText = encryptor.encrypt(text)
    return (key, cipherText)


def RC4_decrypt(data):
    (key, cipherText) = data
    decryptor = ARC4.new(key=key)
    plainText = decryptor.decrypt(cipherText)
    return plainText


# -------------------------------------
# Asymmetric Key (Elliptic Curve, RSA)
# -------------------------------------

FIPS = "fips-186-3"


# Pycryptodome may be broken
def ECC_encrypt(text):
    key = ECC.generate(curve='P-256')
    public_key = key.public_key()
    hashText = SHA256.new(text)
    signer = DSS.new(key, FIPS)
    sig = signer.sign(hashText)
    return (text, public_key, sig)


def ECC_decrypt(data):
    (text, public_key, sig) = data
    hashVerifier = SHA256.new(text)
    verifier = DSS.new(public_key, FIPS)

    # Per documentation this should not pass, 
    #     but they created example code to bypass...
    # This shouldn't be the case
    verified = True
    try:
        verifier.verify(hashVerifier, sig)
    except ValueError:
        print ("The message is not authentic.")
        verified = False

    return verified


def RSA_encrypt(text):
    key = RSA.generate(2048)
    public_key = key.publickey()
    encryptor = PKCS1_OAEP.new(public_key)
    cipherText = b''
    position = 0
    while position < len(text):
        cipherText = cipherText + encryptor.encrypt(
            text[position:position + 214]
        )
        position = position + 214
    return (key, cipherText)


def RSA_decrypt(data):
    (key, cipherText) = data
    decryptor = PKCS1_OAEP.new(key)
    plainText = b""
    position = 0
    while position < len(cipherText):
        plainText = plainText + decryptor.decrypt(
            cipherText[position:position + 256],
        )
        position = position + 256
    return plainText


# -------------------------------------
# Hash Function (MD5, SHA512)
# -------------------------------------

def _hash(fn, text):
    hashText = fn.new(text).hexdigest()
    return hashText


def MD5(text):
    import Cryptodome.Hash.MD5 as _MD5
    return _hash(_MD5, text)


def SHA512(text):
    import Cryptodome.Hash.SHA512 as _SHA512
    return _hash(_SHA512, text)
