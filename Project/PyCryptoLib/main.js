#!/usr/bin/env node

const
    _         = require("lodash"),
    cryptoLib = require("./cryptoLib"),
    print     = console.log;

const
    IFILE  = "fortunes",
    TEXT   = require("fs").readFileSync(IFILE),
    TRIALS = 5,
    CRYPT  = {
        "AES"     : [cryptoLib.AES_encrypt, cryptoLib.AES_decrypt],
        "3DES"    : [cryptoLib.DES_encrypt, cryptoLib.DES_decrypt],
        "ChaCha20": [cryptoLib.CHA_encrypt, cryptoLib.CHA_decrypt],
        "RC4"     : [cryptoLib.RC4_encrypt, cryptoLib.RC4_decrypt],
        "ECC"     : [cryptoLib.ECC_sign, cryptoLib.ECC_verify],
        "RSA"     : [cryptoLib.RSA_encrypt, cryptoLib.RSA_decrypt],
    },
    HASH   = {
        "MD5"   : cryptoLib.MD5,
        "SHA512": cryptoLib.SHA512,
    };

function _printHeader (tag) {
    const line = "-".repeat(70);
    print(`${line}\n${tag}\n${line}\n`);
}

function _execute (fn, data) {
    // https://nodejs.org/api/process.html#process_process_hrtime_time
    // [seconds, nanoseconds]
    // nanoseconds = remaining part of the real time that can't be represented in second precision.
    const
        time   = process.hrtime(),
        result = fn(data),
        diff   = process.hrtime(time),
        min    = (Math.floor(diff[0] / 60)).toString().padStart(2, "0"),
        sec    = (diff[0] % 60).toString().padStart(2, "0"),
        ms     = Math.floor(diff[1] / 1000).toString().padStart(6, "0"),
        elapse = `${min}:${sec}.${ms}`;
    return [result, elapse]
}

function run () {
    print(`length text = ${TEXT.length}\n`);

    _printHeader("Encryption / Decryption");
    _.each(CRYPT, ([encrypt, decrypt], algo) => {
        print(`${algo}\n`);
        _.each(_.range(TRIALS), () => {
            let [encData, elapse1] = _execute(encrypt, TEXT);
            print(`  encrypt : ${elapse1} m:s.ms`);
            let [data, elapse2] = _execute(decrypt, encData);
            print(`  decrypt : ${elapse2} m:s.ms`);
        });
        print();
    });

    _printHeader("Hash");
    _.each(HASH, (fn, algo) => {
        print(`${algo}\n`);
        _.each(_.range(TRIALS), () => {
            const [data, elapse] = _execute(fn, TEXT);
            print(`  hash : ${elapse} m:s.ms`);
        });
        print();
    });
}

run();
