# Overview

The final project will be an extensive comparison of encryption algorithms.
There are two parts to this project.

## 1. Implementation
+ Using any language that you want (assuming you can complete the assignment in
  it), find at least 3 crypto libraries and write a program that utilizes the
  libraries.

    + If the language you initially chose turns out to not have 3 different
      crypto libraries, you may use a second language for your 3rd crypto
      library.

    + Likewise, if your crypto libraries don't support all the functions you
      need, you may use more than 3.

+ You will encrypt/decrypt/hash a file of at least 5 MB (this may take a really
  long time with some public key algorithms) with each library and each
  algorithm and compare the performance results.

    + Perform each encryption and decryption or hash several times and average
      the results for your final value for each encryption and decryption (keep
      the values separate) and hash.

    + Do this all on the same computer so as to minimize hardware/system bias.

+ Choose at least 2 algorithms from each of these classifications to test across
  each crypto library.  This will be a minimum of 24 tests.

    + Block cipher
        + DES
        + RC5
        + AES
        + Blowfish
    + Stream cipher
        + RC4
        + Salsa20
        + ChaCha20
        + Rabbit
    + Asymmetric Key
        + Diffie-Hellman key exchange
        + Eliptic Curve Cryptography
        + RSA
        + ElGamal

        If the function you are using doesn't automatically break files into
        blocks for the encryption (you get an error saying that the file is too
        large) then do it manually and just store the encryption in ECB mode.

    + Hash Function
        + SHA (1,2, 256, 512, etc.)
        + MD5

+ For all of these tasks, record your results and graph or chart them.

    + Make sure to include information about the:
        + OS,
        + hardware,
        + and network connection you were using for these experiments.

    + Include this data in a report and discuss any discrepancies between the
      relative ranking of the encryption algorithms between the different
      libraries.

## 2. Analysis
+ Discuss your conclusions about which which algorithms and libraries you
  consider to be the best to use.

    + Include factors such as:
        + the ease of use/implementation,
        + the availability of documentation,
        + the support for a variety of algorithms,
        + along with performance in your assessment.

+ Submit your code (well documented, as usual) and your report in a zip or tgz
  file.  Include an install script if you are using any non-standard libraries.
  As usual, this will be executed in ubuntu, so include any apt-get or wget +
  installation commands to acquire the necessary libraries.  Also include a demo
  script to build and execute your code.

    + If you do all or any part of this with windows/mac only libraries, please
      specify which ones they are and create separate and well-labeled scripts
      for those.

## 3. Crypto Libs
+ cryptography: https://github.com/pyca/cryptography
+ pycryptodomex: https://github.com/Legrandin/pycryptodome

## 4. Sample Files
+ Text File: http://www.sample-videos.com/download-sample-text-file.php
+ Image File: http://www.sample-videos.com/download-sample-jpg-image.php
+ Fortunes: https://github.com/ruanyf/fortunes.git
