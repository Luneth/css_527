const crypto = require("crypto");

function _genKey (len) {
    // https://blog.tompawlak.org/generate-random-values-nodejs-javascript
    return crypto
        .randomBytes(Math.ceil(len / 2))
        .toString('hex') // convert to hexadecimal format
        .slice(0, len);   // return required number of characters
}

function _encrypt ([algo, data]) {
    const
        key     = _genKey(32),
        cipher  = crypto.createCipher(algo, key),
        crypted = cipher.update(data, 'utf-8', 'hex') + cipher.final('hex');
    return [algo, key, crypted];
}

function _decrypt ([algo, key, crypted]) {
    const
        decipher  = crypto.createDecipher(algo, key),
        decrypted = decipher.update(crypted, 'hex', 'utf-8') + decipher.final('utf-8');
    return decrypted;
}

const
    AES         = "aes-256-cbc",
    AES_encrypt = (data) => { return _encrypt([AES, data]) },
    AES_decrypt = (args) => { return _decrypt(args) };

const
    DES         = "des3",
    DES_encrypt = (data) => { return _encrypt([DES, data]) },
    DES_decrypt = (args) => { return _decrypt(args) };

const
    // https://github.com/thesimj/js-chacha20
    _cha        = require("js-chacha20"),
    CHA_encrypt = (data) => {
        const key     = new Uint8Array(crypto.randomBytes(32)),
              nonce   = new Uint8Array(crypto.randomBytes(12)),
              encoder = new _cha(key, nonce),
              crypt   = encoder.encrypt(data);
        return [key, nonce, crypt];
    },
    CHA_decrypt = ([key, nonce, crypt]) => {
        const decoder = new _cha(key, nonce);
        return decoder.decrypt(crypt);
    };

const
    RC4         = "rc4",
    RC4_encrypt = (data) => { return _encrypt([RC4, data]) },
    RC4_decrypt = (args) => { return _decrypt(args) };

const
    // https://github.com/jpillora/eccjs
    _ecc       = require("eccjs"),
    ECC_sign   = (data) => {
        const keys = _ecc.generate(_ecc.SIG_VER),
              sig  = _ecc.sign(keys.sig, data);
        return [data, keys, sig];
    },
    ECC_verify = ([data, keys, sig]) => {
        return _ecc.verify(keys.ver, sig, data)
    };

const
    // https://github.com/rzcoder/node-rsa
    _rsa        = require("node-rsa"),
    RSA_encrypt = (data) => {
        const keys  = new _rsa({b: 512}),
              crypt = keys.encrypt(data, "base64");
        return [keys, crypt];
    },
    RSA_decrypt = ([keys, data]) => {
        return keys.decrypt(data, "utf8")
    };

const
    _hash  = (type, text) => { return crypto.createHash(type).update(text).digest("hex") },
    MD5    = (text) => { return _hash("md5", text) },
    SHA512 = (text) => { return _hash("sha512", text) };

module.exports = {
    AES_encrypt, AES_decrypt,
    DES_encrypt, DES_decrypt,
    CHA_encrypt, CHA_decrypt,
    RC4_encrypt, RC4_decrypt,
    ECC_sign, ECC_verify,
    RSA_encrypt, RSA_decrypt,
    MD5, SHA512
};
