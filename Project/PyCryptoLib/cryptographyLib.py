# Seth Pham
# University of Washington
# CSS 527
# Brent Lagesse
# Project: Cryptography Library Analysis

import os

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes, padding
from cryptography.hazmat.primitives.asymmetric import ec, rsa, utils
from cryptography.hazmat.primitives.asymmetric import padding as apadding
from cryptography.hazmat.primitives.ciphers import algorithms, Cipher, modes

BACKEND = default_backend()


# -------------------------------------
# Block Ciphers (AES, 3DES)
# -------------------------------------

def _block_padding(text):
    padder = padding.PKCS7(128).padder()
    padded_data = padder.update(text)
    padded_data += padder.finalize()
    return padded_data


def AES_encrypt(text):
    padded_data = _block_padding(text)
    key = os.urandom(32)
    iv = os.urandom(16)
    cipher = Cipher(algorithms.AES(key), modes.CBC(iv), backend=BACKEND)
    encryptor = cipher.encryptor()
    cipherText = encryptor.update(padded_data) + encryptor.finalize()
    return (key, iv, cipherText)


def AES_decrypt(data):
    (key, iv, cipherText) = data
    cipher = Cipher(algorithms.AES(key), modes.CBC(iv), backend=BACKEND)
    decryptor = cipher.decryptor()
    plainText = decryptor.update(cipherText) + decryptor.finalize()
    return plainText


def DES_encrypt(text):
    padded_data = _block_padding(text)
    key = os.urandom(16)
    iv = os.urandom(8)
    cipher = Cipher(algorithms.TripleDES(key), modes.CBC(iv), backend=BACKEND)
    encryptor = cipher.encryptor()
    cipherText = encryptor.update(padded_data) + encryptor.finalize()
    return (key, iv, cipherText)


def DES_decrypt(data):
    (key, iv, cipherText) = data
    cipher = Cipher(algorithms.TripleDES(key), modes.CBC(iv), backend=BACKEND)
    decryptor = cipher.decryptor()
    plainText = decryptor.update(cipherText) + decryptor.finalize()
    return plainText


# -------------------------------------
# Stream Cipher (ChaCha 20, RC4)
# -------------------------------------

def ChaCha20_encrypt(text):
    key = os.urandom(32)
    nonce = os.urandom(16)
    algorithm = algorithms.ChaCha20(key, nonce)
    cipher = Cipher(algorithm, mode=None, backend=BACKEND)
    encryptor = cipher.encryptor()
    cipherText = encryptor.update(text)
    return (key, nonce, cipherText)


def ChaCha20_decrypt(data):
    (key, nonce, cipherText) = data
    algorithm = algorithms.ChaCha20(key, nonce)
    cipher = Cipher(algorithm, mode=None, backend=BACKEND)
    decryptor = cipher.decryptor()
    plainText = decryptor.update(cipherText)
    return plainText


def RC4_encrypt(text):
    key = os.urandom(32)
    algorithm = algorithms.ARC4(key)
    cipher = Cipher(algorithm, mode=None, backend=BACKEND)
    encryptor = cipher.encryptor()
    cipherText = encryptor.update(text)
    return (key, cipherText)


def RC4_decrypt(data):
    (key, cipherText) = data
    algorithm = algorithms.ARC4(key)
    cipher = Cipher(algorithm, mode=None, backend=BACKEND)
    decryptor = cipher.decryptor()
    plainText = decryptor.update(cipherText)
    return plainText


# -------------------------------------
# Asymmetric Key (Elliptic Curve,RSA)
# -------------------------------------

CHOSEN_HASH = hashes.SHA256()


# this uses Signature Verification
def ECC_encrypt(text):
    private_key = ec.generate_private_key(ec.SECP384R1(), BACKEND)
    hasher = hashes.Hash(CHOSEN_HASH, BACKEND)
    hasher.update(text)
    digest = hasher.finalize()
    sig = private_key.sign(digest, ec.ECDSA(utils.Prehashed(CHOSEN_HASH)))
    public_key = private_key.public_key()
    return (digest, public_key, sig)


def ECC_decrypt(data):
    (digest, public_key, sig) = data
    verified = public_key.verify(
        sig,
        digest,
        ec.ECDSA(utils.Prehashed(CHOSEN_HASH))
    )
    return verified


def RSA_encrypt(text):
    private_key = rsa.generate_private_key(
        public_exponent=65537,
        key_size=2048,
        backend=BACKEND
    )
    public_key = private_key.public_key()

    cipherText = b''
    position = 0
    while position < len(text):
        cipherText = cipherText + public_key.encrypt(
            text[position:position + 214],
            apadding.OAEP(
                mgf=apadding.MGF1(algorithm=hashes.SHA1()),
                algorithm=hashes.SHA1(),
                label=None
            )
        )
        position = position + 214

    return (private_key, cipherText)


def RSA_decrypt(data):
    (private_key, cipherText) = data
    position = 0
    plainText = b''
    while position < len(cipherText):
        plainText = plainText + private_key.decrypt(
            cipherText[position:position + 256],
            apadding.OAEP(
                mgf=apadding.MGF1(algorithm=hashes.SHA1()),
                algorithm=hashes.SHA1(),
                label=None
            )
        )
        position = position + 256
    return plainText


# -------------------------------------
# Hash Function (MD5, SHA512)
# -------------------------------------

def _hash(fn, text):
    digest = hashes.Hash(fn(), backend=BACKEND)
    digest.update(text)
    hashText = digest.finalize()
    return hashText


def MD5(text):
    return _hash(hashes.MD5, text)


def SHA512(text):
    return _hash(hashes.SHA512, text)
