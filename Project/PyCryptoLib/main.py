#!/usr/bin/env python3

import cryptographyLib
import pycryptodomeLib
import hashLib
import datetime

IFILE = "fortunes"
TRIALS = 5
NOW = datetime.datetime.now

CRYPT = {
    "AES": [
        ("cryptography", cryptographyLib.AES_encrypt, cryptographyLib.AES_decrypt),
        ("pycryptodome", pycryptodomeLib.AES_encrypt, pycryptodomeLib.AES_decrypt),
    ],
    "3DES": [
        ("cryptography", cryptographyLib.DES_encrypt, cryptographyLib.DES_decrypt),
        ("pycryptodome", pycryptodomeLib.DES_encrypt, pycryptodomeLib.DES_decrypt),
    ],
    "ChaCha20": [
        ("cryptography", cryptographyLib.ChaCha20_encrypt, cryptographyLib.ChaCha20_decrypt),
        ("pycryptodome", pycryptodomeLib.ChaCha20_encrypt, pycryptodomeLib.ChaCha20_decrypt),
    ],
    "RC4": [
        ("cryptography", cryptographyLib.RC4_encrypt, cryptographyLib.RC4_decrypt),
        ("pycryptodome", pycryptodomeLib.RC4_encrypt, pycryptodomeLib.RC4_decrypt),
    ],
    "ECC": [
        ("cryptography", cryptographyLib.ECC_encrypt, cryptographyLib.ECC_decrypt),
        ("pycryptodome", pycryptodomeLib.ECC_encrypt, pycryptodomeLib.ECC_decrypt),
    ],
    "RSA": [
        ("cryptography", cryptographyLib.RSA_encrypt, cryptographyLib.RSA_decrypt),
        ("pycryptodome", pycryptodomeLib.RSA_encrypt, pycryptodomeLib.RSA_decrypt),
    ],
}

HASH = {
    "MD5": [
        ("cryptography", cryptographyLib.MD5),
        ("pycryptodome", pycryptodomeLib.MD5),
        ("hashlib", hashLib.MD5),
    ],
    "SHA512": [
        ("cryptography", cryptographyLib.SHA512),
        ("pycryptodome", pycryptodomeLib.SHA512),
        ("hashlib", hashLib.SHA512),
    ]
}


def _getText():
    file = open(IFILE, "rb")
    text = file.read()
    file.close()
    return text


def _printHeader(tag):
    line = "-" * 70
    print("{}\n{}\n{}\n".format(line, tag, line))


def _execute(fn, data):
    startTime = NOW()
    data = fn(data)
    endTime = NOW()
    return (data, endTime - startTime)


def run():
    TAB = "  "
    text = _getText()
    print("length text = {}\n".format(len(text)))

    _printHeader("Encryption / Decryption")
    for algo in CRYPT:
        print("{}\n".format(algo))
        for (lib, encrypt, decrypt) in CRYPT[algo]:
            print("{}{}".format(TAB, lib))
            for _ in range(TRIALS):
                (data, elapse) = _execute(encrypt, text)
                print("{}encrypt : {} m:s.ms".format(TAB * 2, str(elapse)[2:]))
                (data, elapse) = _execute(decrypt, data)
                print("{}decrypt : {} m:s.ms".format(TAB * 2, str(elapse)[2:]))
            print()

    _printHeader("Hash")
    for algo in HASH:
        print("{}\n".format(algo))
        for (lib, hash) in HASH[algo]:
            print("{}{}".format(TAB, lib))
            for _ in range(TRIALS):
                (data, elapse) = _execute(hash, text)
                print("{}hash : {} m:s.ms".format(TAB * 2, str(elapse)[2:]))
            print()


run()
