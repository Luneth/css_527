# Lecture 10/18/17
## CSS 527
## Week 4

### Lecture Slides
* "css527KeyExchange.ppt"
	* Diffie-Hellman
	* El-Gamal
* "Hashing and Message Authentication.pptx"

### Possible Quiz
* El-Gamal verifying algorithm
* How DH fails in practice
* Why must DH have two different key pairs?
	* For E/D and S/V
* Drawing efficient protocol diagrams
	* When to sign, or encrypt correctly