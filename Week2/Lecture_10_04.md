10/04/2017

## Notes on Improvement
* Take notes on each slide and enter into notes

### General Notes
* "Symmetric Crypto, pt2.pptx"
* Side-Channel Attacks
	* power, timing, cache
	* Utilizing the surroundings to figure out the encryption
	* "Encryption key side-channel attack"

* "FiniteFields.pptx"
	* Discrete Math Overview
	* Additional Reading
	* Various Lecture Notes --> Lectures 4-7