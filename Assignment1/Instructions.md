      You will be implementing the 3DES encryption algorithm using keying option 1 with ECB, CBC, and OFB modes as options.   

      Your program should perform the following tasks and take in the following inputs at the command line

      ./3des genkey password keyFile

      ./3des encrypt inputFile keyFile outputFile mode

      ./3des decrypt inputFile keyFile outputFile mode

      Where genKey creates the  3DES encryption keys from the password and stores it in the file input as the keyFile, encrypt encrypts the contents of inputFile with the key in keyFile and stores the result in outputFile, and decrypt takes the data in inputFile and decrypts it using the key in keyFile and stores the result in outputFile.  Do not use existing crypto libraries to perform any of the functions other than to hash the password and derive your keys from the hash.  Truncate the output so that it is the appropriate key size if you use an algorithm that outputs a larger value.

      Block ciphers require you to pad your input, so if the data you are encrypting is not evenly divisible by 64 bits, then you will add padding bits.  We will use PKCS#5 padding.  This involves all the padding bytes being the value of the number of padding bytes you are adding.  You may assume that you only need to pad in whole bytes.  For example, if I am encrypting one byte, FF, then my padded version would be FF 07 07 07 07 07 07 07.  If I was padding two bytes of data, 06 06, then my padded version would be 06 06 06 06 06 06 06 06.

      For CBC mode you will have to include an initialization vector.  This value is supposed to be unique in the sense that it is never the same value twice for the same DES key.  You may use the standard rand() function with the time as a seed for this assignment to generate the IV.  The IV should be the size of a block.  You may either generate it as the first 64 bits of your encrypted file or write it to a separate file (as long as your DES decrypt function knows where to look).  The same idea holds for the nonce/IV in OFB mode.

      You may use any language that you want, though keep in mind that I will have a harder time helping you if you choose an esoteric language.   I will be executing your code in Ubuntu 17.04.  If you use an uncommon language (it's safe to use C/C++/Java/Python), make sure to include a "sudo aptitude install ...." to install the compiler for that language in your demo script.   

       

      You will submit the following on canvas in a zip or tgz file:

      1)  The source code, properly documented and formatted

      2)  A script that compiles and demonstrates the execution of the code (and any supporting files such as the file that is to be encrypted)

      3)  If you've made any non-standard design choices or were not able to completely get everything working, please include a writeup that explains what is working and what any difference between your implementation and the standard is so that I can give you partial credit.

       

       

       

       

      For reference, here is the DES algorithm:

       

       

      DES algorithm:

      Input: 
         T: 64 bits of clear text
         k1, k2, ..., k16: 16 round keys
         IP: Initial permutation
         FP: Final permutation
         f(): Round function
         
      Output: 
         C: 64 bits of cipher text

      Algorithm:
         T' = IP(T), applying initial permutation
         (L0, R0) = T', dividing T' into two 32-bit parts
         (L1, R1) = (R0, L0 ^ f(R0, k1))
         (L2, R2) = (R1, L1 ^ f(R1, k2))
         ......
         C' = (R16, L16), swapping the two parts
         C = FP(C'), applying final permutation
      where ^ is the XOR operation.

      The round function f(R,k) is defined as:

      Input: 
         R: 32-bit input data
         k: 48-bit round key
         E: Expansion permutation
         P: Round permutation
         s(): S boxes function
         
      Output
         R' = f(R,k): 32-bit output data

      Algorithm
         X = E(R), applying expansion permutation and returning 48-bit data
         X' = X ^ k, XOR with the round key
         X" = s(X'), applying S boxes function and returning 32-bit data
         R' = P(X"), applying the round permutation
      The S boxes function s(X) is defined as:

      Input: 
         X: 48-bit input data
         S1, S2, ..., S8: 8 S boxes - 4 x 16 tables

      Output: 
         X' = s(X): 32-bit output data

      Algorithm: 
         (X1, X2, ..., X8) = X, dividing X into 8 6-bit parts
         X' = (S1(X1), S2(X2), ..., S8(X8))
            where Si(Xi) is the value at row r and column c of S box i with
               r = 2*b1 + b6 
               c = 8*b2 + 4*b3 + 2*b4 + b5 
               b1, b2, b3, b4, b5, b6 are the 6 bits of the Xi 
      DES cipher algorithm supporting tables:

      Initial Permutation - IP:

      58    50   42    34    26   18    10    2
      60    52   44    36    28   20    12    4
      62    54   46    38    30   22    14    6
      64    56   48    40    32   24    16    8
      57    49   41    33    25   17     9    1
      59    51   43    35    27   19    11    3
      61    53   45    37    29   21    13    5
      63    55   47    39    31   23    15    7
      Final Permutation - FP:

      40     8   48    16    56   24    64   32
      39     7   47    15    55   23    63   31
      38     6   46    14    54   22    62   30
      37     5   45    13    53   21    61   29
      36     4   44    12    52   20    60   28
      35     3   43    11    51   19    59   27
      34     2   42    10    50   18    58   26
      33     1   41     9    49   17    57   25
      Expansion permutation - E:

      32     1    2     3     4    5
       4     5    6     7     8    9
       8     9   10    11    12   13
      12    13   14    15    16   17
      16    17   18    19    20   21
      20    21   22    23    24   25
      24    25   26    27    28   29
      28    29   30    31    32    1
      Round permutation - P:

      16   7  20  21
      29  12  28  17
       1  15  23  26
       5  18  31  10
       2   8  24  14
      32  27   3   9
      19  13  30   6
      22  11   4  25
      S boxes - S1, S2, ..., S8:

                              S1

      14  4  13  1   2 15  11  8   3 10   6 12   5  9   0  7
       0 15   7  4  14  2  13  1  10  6  12 11   9  5   3  8
       4  1  14  8  13  6   2 11  15 12   9  7   3 10   5  0
      15 12   8  2   4  9   1  7   5 11   3 14  10  0   6 13

                              S2

      15  1   8 14   6 11   3  4   9  7   2 13  12  0   5 10
       3 13   4  7  15  2   8 14  12  0   1 10   6  9  11  5
       0 14   7 11  10  4  13  1   5  8  12  6   9  3   2 15
      13  8  10  1   3 15   4  2  11  6   7 12   0  5  14  9

                              S3

      10  0   9 14   6  3  15  5   1 13  12  7  11  4   2  8
      13  7   0  9   3  4   6 10   2  8   5 14  12 11  15  1
      13  6   4  9   8 15   3  0  11  1   2 12   5 10  14  7
       1 10  13  0   6  9   8  7   4 15  14  3  11  5   2 12

                              S4

       7 13  14  3   0  6   9 10   1  2   8  5  11 12   4 15
      13  8  11  5   6 15   0  3   4  7   2 12   1 10  14  9
      10  6   9  0  12 11   7 13  15  1   3 14   5  2   8  4
       3 15   0  6  10  1  13  8   9  4   5 11  12  7   2 14

                              S5

       2 12   4  1   7 10  11  6   8  5   3 15  13  0  14  9
      14 11   2 12   4  7  13  1   5  0  15 10   3  9   8  6
       4  2   1 11  10 13   7  8  15  9  12  5   6  3   0 14
      11  8  12  7   1 14   2 13   6 15   0  9  10  4   5  3

                              S6

      12  1  10 15   9  2   6  8   0 13   3  4  14  7   5 11
      10 15   4  2   7 12   9  5   6  1  13 14   0 11   3  8
       9 14  15  5   2  8  12  3   7  0   4 10   1 13  11  6
       4  3   2 12   9  5  15 10  11 14   1  7   6  0   8 13

                              S7

       4 11   2 14  15  0   8 13   3 12   9  7   5 10   6  1
      13  0  11  7   4  9   1 10  14  3   5 12   2 15   8  6
       1  4  11 13  12  3   7 14  10 15   6  8   0  5   9  2
       6 11  13  8   1  4  10  7   9  5   0 15  14  2   3 12

                              S8

      13  2   8  4   6 15  11  1  10  9   3 14   5  0  12  7
       1 15  13  8  10  3   7  4  12  5   6 11   0 14   9  2
       7 11   4  1   9 12  14  2   0  6  10 13  15  3   5  8
       2  1  14  7   4 10   8 13  15 12   9  0   3  5   6 11