import hashlib


# Generate a 192 bit key (three 64 bit keys)
def genHashFromPsw(password):
    bytes = password.encode()
    hash = hashlib.sha256(bytes)
    digest = hash.hexdigest()[:(192 // 4)]
    return [digest[:16], digest[16:32], digest[32:]]


# Store in keyFile
def storeKeys(hashes, keyFile):
    file = open(keyFile, 'w')
    for hash in hashes:
        print(hash)
        file.write("%s\n" % hash)


# args = {'func': 'genkey', 'password': 'password', 'keyFile': 'keyFile'}
def genkey(args):
    print(args)
    psw = args["password"]
    hashes = genHashFromPsw(psw)
    storeKeys(hashes, args["keyFile"])
