import common
import tables


# CBC: Plaintext XOR Initial Vector BEFORE Encrypt
# Ciphertext 1 --> decrypt --> XOR Original Initalization Vector
# Ciphertext 2 --> decrpyt --> XOR Ciphertext 1
# Ciphertext 3 --> decrpyt --> XOR Ciphertext 2
def cbc(subKeys, blocks):
    print("CBC")


# ECB: Plaintext Encrypted
# Same as Encrypt, except on 28 bit key halves' shift
# Now we shift to the RIGHT
def ecb(subKeys, blocks):
    results = []
    for block in blocks:
        blockIP = tables.firstPermutation(block)
        for subKey in subKeys:
            blockIP = tables.mangleDecrypt(blockIP, subKey)
        text = list(map(lambda x: blockIP[x], tables.fp))
        results.append(text)
    return results


# OFB: Initial Vector Encrypt THEN Plaintext XOR Encrypted Init Vector
# Initial Vector --> Encrypt --> XOR Ciphertext 1
# Encrypted Init Vector --> Encrypt --> XOR Ciphertext 2
def ofb(subKeys, blocks):
    print("OFB")


MODES = {"CBC": cbc, "ECB": ecb, "OFB": ofb}


def decrypt(subKeys, str, mode):
    blocks = common.split64(str)
    bitsList = MODES[mode](subKeys, blocks)
    text = tables.bin2hex(bitsList)
    # text = ''.join([chr(int(''.join(c), 16)) for c in zip(hexText[0::2], hexText[1::2])])
    # plainText = unpad(text)
    return text
