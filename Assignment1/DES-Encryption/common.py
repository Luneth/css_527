import tables


def getKeys(keyFile):
    file = open(keyFile, 'r')
    keys = file.read().splitlines()
    if len(keys) != 3:
        file.seek(0)
        print("invalid number of keys : \n" + file.read())
        exit()
    for key in keys:
        if len(key) != 16:
            print("invalid key : " + key)
            exit()
    print("keys: " + ",".join(keys))
    return keys


def leftRotate(elems, num):
    for i in range(num):
        x = elems[0]
        elems.append(x)
        del elems[0]
    return elems


## Generate Round keys:
# Input 64 bit key
# Pass through PC-1 lookup table --> 56 bit key
def gen64to56(key):
    bin64 = tables.hex2bin(key)
    bin56 = list(map(lambda x: bin64[x], tables.pc1))
    return bin56


# Split into 28 bit halves (Li and Ri)
# Shift bits to the left based on which round (1-16)
# Save L(1-16) and R(1-16)
# Amount to shift: [1,1,2,2,2,2,2,2,1,2,2,2,2,2,2,1] (1-16)
# Concatenate Li and Ri
# Pass through PC-2 lookup table --> 48 bit subkey (1-16)
def gen16Subkeys(key_64):
    key_56 = gen64to56(key_64)
    left = key_56[:28]
    right = key_56[28:]
    subKeys = []
    for r in tables.ROTATIONS:
        left = leftRotate(left, r)
        right = leftRotate(right, r)
        whole = left + right
        subKey = list(map(lambda x: whole[x], tables.pc2))
        subKeys.append(subKey)
    return subKeys


def genSubkeys(keys_64):
    subkeys = list(map(lambda x: gen16Subkeys(x), keys_64))
    return subkeys


def reverseSubkeys(subKeys):
    results = []
    for keys in subKeys:
        rev = list(reversed(keys))
        results.append(rev)
    return results


PADS = [
    "",
    "07" * 7,
    "06" * 6,
    "05" * 5,
    "04" * 4,
    "03" * 3,
    "02" * 2,
    "01",
]


def pad(str):
    num = (len(str) % 16) // 2
    pad = PADS[num]
    return str + pad


def split64(str):
    results = []
    while str != "":
        s = pad(str[:16])
        results.append(s)
        str = str[16:]
    return results


def saveFile(text, outputFile):
    file = open(outputFile, 'w')
    file.write(text)
