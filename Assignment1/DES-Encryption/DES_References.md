### DES Link Reference
+ [DES Walkthrough](https://www.cs.bham.ac.uk/research/projects/lemsys/DES/DESPage.jsp)
+ [Triple DES Keys](https://www.cryptosys.net/3des.html)
+ [Round Key Rotation](https://crypto.stackexchange.com/questions/33072/how-is-a-per-round-key-generated-in-des-algorithm)
+ [ECB and CBC Diagram](http://manansingh.github.io/Cryptolab-Offline/c13-des-modes.html)
+ [OFB diagram (all modes)](https://en.wikipedia.org/wiki/Block_cipher_mode_of_operation)
+ [BitArray](https://wiki.python.org/moin/BitArrays)
+ [Bit Substitution](https://stackoverflow.com/questions/12173774/modify-bits-in-an-integer-in-python)