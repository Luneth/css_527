import common
import tables


# CBC: Plaintext XOR Initial Vector BEFORE Encrypt
def cbc(subKeys, blocks):
    print("CBC")


# ECB: Plaintext Encrypted
def ecb(subKeys, blocks):
    results = []
    for block in blocks:
        blockIP = tables.firstPermutation(block)
        for subKey in subKeys:
            blockIP = tables.mangleEncrypt(blockIP, subKey)
        cipherText = list(map(lambda x: blockIP[x], tables.fp))
        results.append(cipherText)
    return results


# OFB: Initial Vector Encrypt THEN Plaintext XOR Encrypted Init Vector
def ofb(subKeys, blocks):
    print("OFB")


MODES = {"CBC": cbc, "ECB": ecb, "OFB": ofb}


def encrypt(subKeys, str, mode):
    blocks = common.split64(str)
    bitsList = MODES[mode](subKeys, blocks)
    text = tables.bin2hex(bitsList)
    return text
