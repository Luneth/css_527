Notes:
+ 8 or so sources
+ At least the top results
+ At least 5 pages
+ Categories:
	+ General overview of papers
	+ Categorize based on field of research
		+ Maybe focus on the sections of the encryption
+ Sections:
	+ Introduction
	+ Summary of Papers
		+ keep compare/contrast out
		+ need distinct headers
	+ Compare/contrast section
	Could do 1 or more (based on uniqueness of data)
		+ Either based on encryption sections
		+ Areas of research
		+ Attack vectors
	+ Conclusion
		+ Interesting results