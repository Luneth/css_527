# Class Introductions
** 09/27/2017 **
** Week 0 **

** Note **
	Bring notebook next time to class. Experimenting with using sublime for note taking.
	_Hypothesis:_ Not going to be great for drawings
	_Possible Answer:_ Use Sublime for note review
	Go through the accessory files in Canvas

* 1st half focus: crypto primitives
* 2nd half focus: how to use those
* Primarily crypto analysis

### Coursework
* symmetric key algorithm
* public key crypto algorithm
* hashing-system (sha-256)
* research project
	* 10-15 research papers and then a taxiconomy
* final project (coding)
	* use 20-30 crypto libraries for basic crypto
	* "what was good, what was bad?"
* quizzes
	* beginning of class
	* up from last Monday's class
	* short-answers (1-3 sentences)
	* **make-up**, give 3 day warning


### Advice
* Start Early
* No extensions 
* Try a new language (Python)

### Homework 1
* Implement 3DES with CBC, ECB, and CTR modes and a password -> key generator
* A lot of look-up tables (static)
	* **DO USE** other's use of look-up tables

### Research Project
* Write a survey paper of an area related to cryptography
	* don't summarize papers, systemize the knowledges
	* 3-4 pages
	* LaTex

### ACM Computing Surveys
To quickly learn about a new field