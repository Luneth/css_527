# Purpose of Crypto
** Week 0 **
** 09/27/17 **

** Search: "How I met your girlfriend - BlackHat" **

### Purpose of Crypto
Securely process, store, and transmit data
Start with crypto primitives

### Overview
Symmetric
	* Block cipher
		* Ex: AES, (3)DES
	* Stream cipher
		* Ex: RC4, Salsa20
		* Fast but easy to mess-up
		* XOR
		* stream of bits must be impossible to guess
Assymmetric
	* RSA
	* DH
Hash
	* Md5 (NEVER use)
	* SHA
	* HMAC
Library
	* crypto
	* pycrypt++
	* crypto++
	* bomcy castle
	* capi (old windows code)
	* CNG (newer windows code)

### Some Notation
* Names are used to set-up a scenario for demonstration
	* **Alice and Bob** = secure communication, **Eve** = Eavesdropper, **Mallory** = malicious
* Example:
		A 					B
		c = E(k,msg) --(c)--> D(k,c) = msg

### More Notation
* Algorithm A(.) denotes an algorithm with one input, A(.,.) with two, etc. A(x) represents a probablility distribution. 
	* If A is deterministic, then the distribution is a single element
* We sample 'x' from a distribution, S with x <- S. If F is a finite set, then x <- F implies that 'x' is sampled uniformly from F.
* p(.,.) denotes a predicate
* Semicolons denote a new step is occurring
* Example: 
		- Pr[x <- S; (y,z) <- A(x): p(y,z)]
		- Probability that the predicate p(y,z) is true after x is sampled from S then provided as an input to algorithm A with output (y,x)
		- **Purpose:** Given all the information within, what is the probability of an attacker learning the original message

		S = {0,1}
		x <- S
			x = 0 50%
			    1 50%

### Definitions
* An algorithm is a Turing machine whose input/output (i/o) are strings over some alphabet
* An algorithm A runs in time T(n) if for all x e B*, A(x) halts within T(|x|) steps. _("e" = "belongs to")_
	* polynomial time if there is a constant c such that A runs in time T(n) = n^c.

### Some Requirements
* It should be hard to recover message from ciphertext
	* to compute partial info about the msg
	* to detect simple facts about msg traffic

### Model of the Adversary
* XYZ is secure is seldom a statement we can make
* Common assumptions:
	* Can see anything we send (insecure channel)
	* Can perform probabilistic polynomial time computation
* Passive vs Active
	* Delete or modify ciphertexts
	* Request a large number of decryptions
	* Resend a copy of a cyphertext

### What is Perfect Security?
* The message should have a statistically independent encoded output (low probability correlation)
* Security = How randomly are the keys generated
* If you change keys for every message --> you break statistical dependence
	* Downside: You have to agree on how many messages you are going to send before-hand

### What is more practical?
* The algorithm is public, but the key is secret
	- key must be big
	- To have perfect security, your key would need to be at least the size of any possible message. (not feasible)
* Design a keyspace large enough that computers are not fast enough to decypher the key

### Breaking Crypto
* It is possible to efficiently break some cryptosystems
* Is it possible to efficiently break any cryptosystem? (Unknown)
* The problem is tied to the existence of one-way funcitons
	* If they exist, then P!= NP, and symmetric key encryption offers storng security guarantees
	* If they do not exist, then only one-time pads are secure

### Semantic Security
* Rather than making a system perfectly secure, we can make it semantically secure
* This effectively means that knowledge of a ciphertext and length of an unknown msg does not feasibly reveal any additional information

### Negligible Function
* if the function f(x) shrinks faster than '1/(n^c)' 
	- '1/(n^c)' the leakage
		Pr[y e f(x)]
		- Pr[x e A(y)]
		= 1/n^c ---> 0 is ideal

### Random Numbers
* Randomness is essential
* Some random number generators are not random enough