# Lecture 10/23/17
## CSS 527
## Week 4

### Lecture Slides
+ "Hashing and Message Authentication.pptx"

### Possible Quiz


### Assignment 2: SHA and Hash


### SHA family
+ SHA-0,1,2,3(3 is not maed by NSA)
+ <shattered.io> watch SHA-1 being broken

### SHA-512
+ [Diagram]
+ Why does the Actual Message have "Padding + Length"
	+ The Length and Padding are hashed with the message
	+ The Length tells

### Padding
+ The last 128 bits are message length (unsigned `int`, MSB comes first)
+ Padding consists of a 1 followed by all 0s as required
+ The Padding can be in the front or the end
	+ The Length must always be at the end
+ Even if no padding, must add Length -->
	+ Which means you will have a 896 Block of padding + 128 Bits of Length

### Hash Buffer/IV
+ Hard coded register to be used
+ 8 64-bit registers (a-h)

### Processing (f)
+ Each block involves 80 rounds

### Calculating the Message Schedule
+ Large amount of bits touching other bits
+ Two rotations and a shift
	+ Rotate: bits will loop around
	+ Shift:  bits will drop off at the end, 0s populate the other end

### Round Function
+ Creates diffusion
+ Confusion comes from the Hashing
+ We WANT this to be deterministic

### Calculating T1 and T2
+ This is where the `W` and `K` are incorporated
+ This is all done on bitwise functions.
+ Uses the original values before being re-assigned
+ `Addition modulo 64 = +64`
	+ 1101 XOR 0111 = 1010
	+ 1101 +16 0111 = 0100 --> 13+7=20 --> 20 mod 16 = 4 --> 0100

### Finally
+ Output of the 80th round + hash buffer from the beginning for each register
+ The final content of the hash buffer is the message digest

### Salt
+ Forces the attacker to hold more tables for Rainbow Tables
	+ Not only for the passwords, but now for a salt added to each password
+ H(pass | salt), salt ----> pswdFile [h1,salt]

### Hashed Message Authentication Code (HMAC)
+ Crease a MAC using a hashing function
	+ can also be made for block and stream ciphers
+ `Why do this?`
	+ Computation time
		+ Public key cryptography is way slower
	+ Good for large number of messages
		+ Used after the initial communication

### HMAC Implementation
+ Take a `key K` pad it with 0s to  = the length of a message block (b bits) ---> `K'`
+ Construct ipad and opad (Chosen so that 50% of the bits were different)
	+ `Assignment 2`  ipad and opad are given
+ `HMAC(Y) = h ((K' xor opad) || h((K' xor ipad) || Y))`
	+ `Y` is the message in the previous slide
+ Where h is the underlying hashing function

## TOPIC SHIFT

## Network Design Review

### Layers
1. Application
	+ language apps speak
	+ Tells what you need to do based on responses
2. Transport
	+ app to app
	+ port number
	+ TCP or UDP
		+ TCP large overhead, trusted/certifying packet
3. Network
	+ device to device across networks
	+ IP address
	+ Internet Protocol
4. Data Link
	+ device to device on same network
	+ MAC address
	+ how to handle transmission collision
	+ WiFI: RTS(request to send), CTS(clear to send)
5. Physical
	+ bits --> physical(voltate/pulses) --> bits

#### Network in Detail
	A ---> O ---> X ---> X---> O ---> B
	host							server
	DL[IP[TCP[GET]]]
		Gets to Router [IP[TCP[GET]]]
		Reads and assess next destination
		Router now adds layer [DL[IP[TCP[GET]]]]
	Repeats until reaching B server
		Reads IP and passes that to the computer
			[TCP[GET]]
	Computer takes the TCP and runs App
	Returns the message back
### Comments/Issues
+ Protocol compatibility
	+ Due to the RFC guidelines having "should" and "may" some follow the guides, some do no
+ Encrypting the data at several layers may not actually increase security, and possibly heavily hinder performance
	+ Unless you are assuming every stack is breached
+ Your control is limited to what layer you are programming in