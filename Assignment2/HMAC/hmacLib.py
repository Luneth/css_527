"""
HMAC: Hashed Message Authentication Code

. Pad the key K with 0s on the right to make it the length of a message block
  (b bits) – K+

. Construct ipad and opad (Chosen so that 50% of the bits were different,
  simplicity of implementation)
  . ipad -> repeat 00110110 b/8 times
  . opad -> repeat 01011100 b/8 times

. HMAC(Y) = h ((K+ xor opad) || h((K+ xor ipad) || Y))
  . Y is the message in the previous slide
  . h is the underlying hashing function
"""

BLOCK_SIZE = 64
IPAD = 0x36
OPAD = 0x5C
IPADS = [IPAD] * BLOCK_SIZE
OPADS = [OPAD] * BLOCK_SIZE


def xor(A, B):
    C = [x ^ y for x, y in zip(A, B)]
    return bytes(C)


def create(key, msg):
    import sha256
    iKeyPad = xor(key, IPADS)
    oKeyPad = xor(key, OPADS)

    iSum = iKeyPad + msg
    iState = sha256.sha256State(iSum)
    iDigest = sha256.digest(iState)

    oSum = oKeyPad + iDigest
    oHex = sha256.sha256Hex(oSum)
    return oHex


def verify(key, msg):
    import hmac, hashlib
    hex = hmac.new(key, msg, hashlib.sha256).hexdigest()
    return hex
