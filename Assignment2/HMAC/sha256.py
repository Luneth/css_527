#!/usr/bin/env python

import binascii
import copy
import string
import struct

KEYS = [
    0x428A2F98, 0x71374491, 0xB5C0FBCF, 0xE9B5DBA5,
    0x3956C25B, 0x59F111F1, 0x923F82A4, 0xAB1C5ED5,
    0xD807AA98, 0x12835B01, 0x243185BE, 0x550C7DC3,
    0x72BE5D74, 0x80DEB1FE, 0x9BDC06A7, 0xC19BF174,
    0xE49B69C1, 0xEFBE4786, 0x0FC19DC6, 0x240CA1CC,
    0x2DE92C6F, 0x4A7484AA, 0x5CB0A9DC, 0x76F988DA,
    0x983E5152, 0xA831C66D, 0xB00327C8, 0xBF597FC7,
    0xC6E00BF3, 0xD5A79147, 0x06CA6351, 0x14292967,
    0x27B70A85, 0x2E1B2138, 0x4D2C6DFC, 0x53380D13,
    0x650A7354, 0x766A0ABB, 0x81C2C92E, 0x92722C85,
    0xA2BFE8A1, 0xA81A664B, 0xC24B8B70, 0xC76C51A3,
    0xD192E819, 0xD6990624, 0xF40E3585, 0x106AA070,
    0x19A4C116, 0x1E376C08, 0x2748774C, 0x34B0BCB5,
    0x391C0CB3, 0x4ED8AA4A, 0x5B9CCA4F, 0x682E6FF3,
    0x748F82EE, 0x78A5636F, 0x84C87814, 0x8CC70208,
    0x90BEFFFA, 0xA4506CEB, 0xBEF9A3F7, 0xC67178F2
]

HASH_BLOCKS = [
    0x6A09E667, 0xBB67AE85, 0x3C6EF372, 0xA54FF53A,
    0x510E527F, 0x9B05688C, 0x1F83D9AB, 0x5BE0CD19
]

BLOCK_SIZE = 64
F8 = 0xFFFFFFFF


def padding(msgLength):
    mdi = msgLength & 0x3F
    length = struct.pack("!Q", msgLength << 3)
    padLength = (55 - mdi) if (mdi < 56) else (119 - mdi)
    return b"\x80" + (b"\x00" * padLength) + length


def rotR(x, y):
    return ((x >> y) | (x << (32 - y))) & F8


def maj(x, y, z):
    return (x & y) ^ (x & z) ^ (y & z)


def ch(x, y, z):
    return (x & y) ^ ((~x) & z)


def initState():
    return {
        "count": 0,
        "buffer": b"",
        "hashBlocks": copy.deepcopy(HASH_BLOCKS)
    }


def computeHash(state, msg):
    w = [0] * 64
    w[0:16] = struct.unpack("!16L", msg)

    for i in range(16, 64):
        s0 = rotR(w[i - 15], 7) ^ rotR(w[i - 15], 18) ^ (w[i - 15] >> 3)
        s1 = rotR(w[i - 2], 17) ^ rotR(w[i - 2], 19) ^ (w[i - 2] >> 10)
        w[i] = (w[i - 16] + s0 + w[i - 7] + s1) & F8

    a, b, c, d, e, f, g, h = state["hashBlocks"]

    for i in range(64):
        s0 = rotR(a, 2) ^ rotR(a, 13) ^ rotR(a, 22)
        t2 = s0 + maj(a, b, c)
        s1 = rotR(e, 6) ^ rotR(e, 11) ^ rotR(e, 25)
        t1 = h + s1 + ch(e, f, g) + KEYS[i] + w[i]
        h = g
        g = f
        f = e
        e = (d + t1) & F8
        d = c
        c = b
        b = a
        a = (t1 + t2) & F8

    state["hashBlocks"] = [(x + y) & F8
                           for x, y in
                           zip(state["hashBlocks"], [a, b, c, d, e, f, g, h])]


def update(state, block):
    state["buffer"] += block
    state["count"] += len(block)
    while len(state["buffer"]) >= 64:
        computeHash(state, state["buffer"][:64])
        state["buffer"] = state["buffer"][64:]


def digest(state):
    update(state, padding(state["count"]))
    packets = [struct.pack("!L", i) for i in state["hashBlocks"]]
    binary = b"".join(packets)
    return binary


def hexDigest(state):
    binary = digest(state)
    hex = binascii.hexlify(binary).decode("ascii")
    return hex


def sha256State(bytesData):
    state = initState()
    update(state, bytesData)
    return state


def sha256Hex(bytesData):
    state = sha256State(bytesData)
    hash = hexDigest(state)
    return hash


if __name__ == "__main__":
    cases = {
        "":
            "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855",
        "password":
            "5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8",
        "hello":
            "2cf24dba5fb0a30e26e83b2ac5b9e29e1b161e5c1fa7425e73043362938b9824",
        "hello" * 5:
            "f7f1c9d01ff8d1bf71d7adf7fd1467cf180c81a2c91109c9a7c280ba38161178",
        string.ascii_letters:
            "3964294b664613798d1a477eb8ad02118b48d0c5738c427613202f2ed123b5f1",
        string.hexdigits:
            "1f55b9dbb34db31ee615fea98af1b62ec6f169bb38355164364099813349e079",
    }

    for str, expected in cases.items():
        print("-" * 64)
        print(str)
        actual = sha256Hex(str.encode("ascii"))
        print(expected)
        print(actual)
        print(expected == actual)
