## You will be implementing an HMAC.

+ First, implement SHA-256 hashing algorithm (The lecture notes describe
  SHA-512. SHA-256 is very similar, but there are a few minor differences (see
  bottom of the page)).

+ Next, Use the hashing algorithm you wrote to implement the HMAC algorithm,
  described in the lecture notes, for a given K.

  ```
  HMAC(M) = h ((K+ xor opad) || h((K+ xor ipad) || M))
  ```

+ Your program should execute as follows
  ```
  ./sha256 password keyFile
  ./hmac create keyFile messageFile outputFile
  ./hmac verify keyFile messageFile outputFile
  ```

  where keyFile is where the key used in the HMAC is stored, messageFile is the
  message to be hashed, and outputFile is the result of the HMAC output.

  Create a key generator like you did in the 3DES assignment to create the key
  from a password except this time you will use your own hashing function to
  generate the key from a password.

  You can store your key and message in whatever format you want.

  The hmac verify function should call a standard hmac library that uses
  sha-256 and compare your output with the output of that implementation to ensure
  that your output was correct.

  **Encode your output in hexadecimal so that it will be easy to compare to
  standard HMAC implementations.**

+ Include comments/documentation that makes it obvious what every part of your
  code is doing.

+ You may use any language that you want, though keep in mind that I will have a
  harder time helping you if you choose an esoteric language.   I will be
  executing your code in Ubuntu 17.04.  If you use an uncommon language (it's safe
  to use C/C++/Java/Python), make sure to include a "sudo aptitude install ...."
  to install the compiler for that language in your demo script.

-----

You will submit the following on canvas in a zip or tgz file:

1. The source code, properly documented and formatted

2. A script that compiles and demonstrates the execution of the code (and any
   supporting files such as the file that is to be hashed)

3. If you've made any non-standard design choices or were not able to completely
   get everything working, please include a writeup that explains what is
   working and what any difference between your implementation and the standard
   for SHA-256/HMAC are so that I can give you partial credit.

-----

The RFC for HMAC is defined at
+ https://tools.ietf.org/html/rfc2104.html

This document should help with hmac intermediate values.
+ http://csrc.nist.gov/groups/ST/toolkit/documents/Examples/HMAC_SHA256.pdf

SHA-256 is defined at
* [SHA256english.pdf](./References/SHA256english.pdf)

-----

### From Wikipedia

SHA-512 is identical in structure to SHA-256, but:

+ the message is broken into 1024-bit chunks,
+ the initial hash values and round constants are extended to 64 bits,
+ there are 80 rounds instead of 64,
+ the message schedule array w has 80 64-bit words instead of 64 32-bit words,
+ to extend the message schedule array w, the loop is from 16 to 79 instead of from 16 to 63,
+ the round constants are based on the first 80 primes 2..409,
+ the word size used for calculations is 64 bits long,
+ the appended length of the message (before pre-processing), in bits, is a 128-bit big-endian integer, and
+ the shift and rotate amounts used are different.
