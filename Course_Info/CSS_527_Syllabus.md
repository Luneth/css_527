# Course Syllabus

## CSS 527 Cryptography and Information Assurance
## Professor Brent Lagesse
### Fall 2017 M/W 5:45-7:45PM

### [Files](https://canvas.uw.edu/courses/1115945/files)
### [Assignments](https://canvas.uw.edu/courses/1115945/assignments)
### [Grades](https://canvas.uw.edu/courses/1115945/grades)
### [People](https://canvas.uw.edu/courses/1115945/users)
 
This course explores symmetric and asymmetric cryptography, key management, and encryption algorithms including DES, AES, RSA, PGP and SSL. Public key infrastructures and virtual private networks are discussed. The course covers protocols, hashing, digital signatures, and certificates and certificate authorities. Cryptanalytic methods are introduced. Policy considerations such as key escrow and recovery and export controls are discussed. Website security issues are addressed as well as electronic commerce security. Issues such as certificate management, certificate revocation cycles, and scalability of PKI solutions are included. Strengths and weaknesses of symmetric and asymmetric cryptography are covered, as well as joint solutions that optimize the strengths of both approaches.

** Email:  lagesse@uw.edu **

 

### Learning Objectives

Define and correctly implement cryptographic protocols for the protection of information at rest and in transit in a software system.
Describe various encryption algorithms such as DES, AES, RSA, PGP and SSL; and various cryptographic-dependent solutions, such as PKI and VPNs.
Describe the life cycle issues associated with cryptographic materials, such as keys, and identify appropriate control measures for each part of the life cycle.
Describe the human factors that impact secure use, administration, maintenance of, and disposal of cryptographic elements in a software system.
Name, define, and describe common attacks on cryptographically secured information.
Describe how cryptography can support the security requirements of a software system and describe where cryptography is inappropriate.


	`Check Canvas regularly.  If you don't get alerts from Canvas, set them up.  This course will be managed through Canvas, so most information will go in the Announcements, Assignments, and Files sections early, and then will be mentioned in class when appropriate.`
 
### Office Hours:  4:30-5:30PM on Monday and Wednesday.
 
 
 
## Grading
** Homework **

20.00%

** Projects **

15.00%

** Research **

15.00%

** Quizzes and Exams **

50.00%

 

## Homework, Research and Projects

Homework assignments are expected to be worked on an individual basis.
Assignment 1 is designed to give the student hands on experience performing encryption and decryption.
Assignment 2 is designed to give the student hands on experience performing hashing functions.
The research segment is designed to give the student an opportunity to explore the state of the art in an area of interest related to cryptography, but not specifically taught in this class.
The final project will be similar to the assignments, but it will provide the student with an opportunity to work on applications and implementations of cryptographic libraries.

** Exams **

The class will have **_ONE_** exam. The exams will be primarily short answer questions related to the terminology, concepts, and practices covered in the class. It will cover all material from the course. A study guide will be provided on the canvas website.

There will be regular **_10-15 minute quizzes_** during this class.  They will generally be once a week and cover the lecture and outside reading for the class from the previous week.  There will be approximately 8-10 quizzes depending on the rate at which material is covered.  Generally expect these will occur on **_Wednesdays at the beginning of class_**, but I will announce any changes to this schedule.

## Required Textbooks
 
Readings will be required on a weekly basis from freely available academic literature
 
## Recommended Textbooks and Readings
 
* Douglas R. Stinson, _Cryptography: Theory and Practice_.
* Charlie Kaufman, Radia Perlman, and Mike Speciner, _Network Security: Private Communication in a Public World._
* Alfred J. Menezes, Paul C. van Oorschot, and Scott A. Vanstone, _Handbook of Applied Cryptography._
* Bruce Schneier, _Applied Cryptography._


## Class Website and Class Notes

Canvas will host the class website where you will find schedule, assignments, grades, and other resources.

## Academic Integrity

You are expected to provide original work based on your own effort for this course. You may not use case studies or past materials from this class as resource. You will be expected to participate as an equal member of a project team in any group projects. You will receive a zero for any exam or coursework for which you are discovered cheating, facilitating, fabricating, or plagiarizing. You may be referred to the University for further action.

## Disability Accommodations

To request academic accommodations due to a disability, please contact Disabled Student Services (DSS) in UW1-175 (Office of Special Services), (425) 352-5000, (425) 352-5303 (TDD). If you have a documented disability on file with the DSS office, please have your DSS counselor contact me and we can discuss accommodations you might need in class.

## Inclement Weather Policy / Campus Status

Call (206)-547-4636 (206-547-INFO) to see if University of Washington, Seattle, Bothell, and Tacoma campuses are open and operating.

 
## Tentative Schedule
The schedule below is subject to change.  Some topics listed may not be covered in lecture, depending on time.

 
#### * No class on Nov 23 due to Thanksgiving. *
 
* Week 0 (Sep 27)
	* Course overview. Symmetric cryptography. Security of symmetric cryptography.
* Week 1 (Oct 2, 4)
	* Classical cryptography. Block ciphers. Cryptanalysis. Building and using block ciphers. DES. AES.
* Week 2 (Oct 9, 11)
	* Stream ciphers. Math background for public key crypto
* Week 3 (Oct 16, 18)
	* Public-key cryptography. RSA. Diffie-Hellman key exchange. ElGamal key agreement.
* Week 4 (Oct 23, 25)
	* Message integrity and authenticity. Digital signature algorithms. Security of digital signatures. DSA. Hash functions: MD5, SHA family.
* Week 5 (Oct 30, Nov 1)
	* Public key infrastructure. X.509. PGP. Key escrow. Policy. Certificate management.
* Week 6 (Nov 6, 8)
	* Isolation for VPNs. MPLS. IPSec.
* Week 7 (Nov 13, 15)
	* Web security. E-commerce. SSL. S/MIME.
* Week 8 (Nov 20)
	* Cryptography in Cloud Computing.
	* Thanksgiving
* Week 9 (Nov 27, 29)
	* Social and Economic aspects of Security (Or why real security is hard)
* Week 10 (Dec 6, 8)
	* Review, Discussion, etc.
Course Summary:
 
## Date	Details
* Wed Oct 4, 2017	
 [Quiz 1](https://canvas.uw.edu/courses/1115945/assignments/3858542)	due by 11:59pm
* Wed Oct 11, 2017	
 [Quiz 2](https://canvas.uw.edu/courses/1115945/assignments/3858544)	due by 11:59pm
* Wed Oct 18, 2017	
 [Quiz 3](https://canvas.uw.edu/courses/1115945/assignments/3898243)	due by 11:59pm
* Sun Oct 22, 2017	
 [Homework 1](https://canvas.uw.edu/courses/1115945/assignments/3858539)	due by 11:59pm
* Wed Oct 25, 2017	
 [Quiz 4](https://canvas.uw.edu/courses/1115945/assignments/3898242)	due by 11:59pm
* Sun Oct 29, 2017	
 [Progress Report](https://canvas.uw.edu/courses/1115945/assignments/3898214)	due by 11:59pm
* Wed Nov 1, 2017	
 [Quiz 5](https://canvas.uw.edu/courses/1115945/assignments/3858545)	due by 11:59pm
* Wed Nov 15, 2017	
 [Quiz 6](https://canvas.uw.edu/courses/1115945/assignments/3858546)	due by 11:59pm
* Sun Nov 19, 2017	
 [Homework 2](https://canvas.uw.edu/courses/1115945/assignments/3858540)	due by 11:59pm
* Wed Nov 22, 2017	
 [Quiz 7](https://canvas.uw.edu/courses/1115945/assignments/3858547)	due by 11:59pm
* Wed Nov 29, 2017	
 [Quiz 8](https://canvas.uw.edu/courses/1115945/assignments/3858548)	due by 11:59pm
* Sun Dec 10, 2017	
 [Project](https://canvas.uw.edu/courses/1115945/assignments/3858541)	due by 11:59pm
 [Research Literature Review](https://canvas.uw.edu/courses/1115945/assignments/3858549)	due by 11:59pm
* Wed Dec 13, 2017	
 [Exam](https://canvas.uw.edu/courses/1115945/assignments/3858538)	due by 11:59pm
