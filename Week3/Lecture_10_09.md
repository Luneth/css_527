### 10/09/17

### Notes
* Reading for next Monday
* Research topic: What has Machine Learning done for cryptography
* How have people used Crypto ideas for Moving Target Defense Techniques

* GSM standards
	(A5/1, A5/2)

### AES Encryption
	symmetric crypto pt2.pptx

### Hacker Thinking
* Compare the hacker's own plaintext, ciphertext and key to figure out the ** encryption **

### Stream Ciphers
	stream ciphers.pptx

### Randome Number generator
Linux

	/dev/random
	blocking

	/dev/urandom
	non-blocking
	"** lower quality ** random numbers, but ** faster **"

## Assignment Notes
	python 3des genkey password keyfile
	"generate the key from a key generator"


	hash(password) --> 256
	"Store this into the keyfile"


	Check for the parity bits
	0 [0011 010][1]
	1 [1011 000][1]
	2 [1100 000][0]
	...
	7
	row: add the 1's, if odd --> add 1 to the end
					  if even --> add 0 to the end